#include<iostream>
#include<cstdio>
#include<vector>
#include<cmath>
#include<cstring>
using namespace std;
int zeros;
void mirror(vector<unsigned char> & vec,int width)
{
	for(int i=width/2+53;i<vec.size();i+=width)//i is middle of each line
	{
		for(int j=i;(j-52)%width!=0;j+=3)//it is doing until arriving to end of line
		{
			int red=vec[j];//red byte of each pixel
			vec[j]=vec[2*(i-1)-j-1];//
			vec[2*(i-1)-j-1]=red;
		
			int green=vec[j+1];//green byte of each pixel
                        vec[j+1]=vec[2*(i-1)-j];
                        vec[2*(i-1)-j]=green;
		
			int blue=vec[j+2];//blue byte of each pixel
 			vec[j+2]=vec[2*(i-1)-j+1];
			vec[2*(i-1)-j+1]=blue;
		}
	}
}
void grayscale(vector<unsigned char> & vec,int width)
{
	for(int i=54;i<vec.size();i+=3)//pixels area
	{
		if((i-52)%width==0)//end of line
		{
			i+=zeros;//going to next line
		}
		int  avg=(vec[i]+vec[i+1]+vec[i+2])/3;//average of pixel's bytes
		vec[i]=avg;//changing the value of bytes to average
		vec[i+1]=avg;
		vec[i+2]=avg; 
	}
}
void rotate(vector<unsigned char> &vec)
{
	int length=(vec[22]+256)%256+((vec[23]+256)%256)*pow(2,8)+((vec[24]+256)%256)*pow(2,16)+((vec[25]+256)%256)*pow(2,24);//length of picture in byte 22 to 25
    int width=(vec[18]+256)%256+((vec[19]+256)%256)*pow(2,8)+((vec[20]+256)%256)*pow(2,16)+((vec[21]+256)%256)*pow(2,24);//width of picture in byte 18 to 21
	width*=3;
	while(width%4!=0){//width should 
		zeros++;
		width++;
	}
	vector<unsigned char> rot(vec.size());//rotated picture
	int k=54;//first byte of pixels
	for(int i=0;i<18;i++)//initializing rotated picture
		rot[i]=vec[i];
	for(int i=18;i<22;i++)//changing length and width
		rot[i]=vec[i+4];
	for(int i=22;i<26;i++)
		rot[i]=vec[i-4];
	for(int i=26;i<54;i++)
		rot[i]=vec[i];
	for(int i=width+51;i>=56;i-=3)//from last pixel to first pixel
	{
		for(int j=0;j<length;j++)//from first line to last line
		{
			rot[k]=vec[(j-1)*width+i-2];//moving each byte of any pixel
			rot[k+1]=vec[(j-1)*width+i-1];
			rot[k+2]=vec[(j-1)*width+i];
			k+=3;//going to next pixel
		}
	}
	for(int j=0;j<vec.size();j++)//changing picture mode to rotated 
                vec[j]=rot[j];
}
int main(int argc,char** argv)
{
        vector<unsigned char> pic;//mian picture
        for(int i=0;i<54;i++)//getting first 54 bytes to finding length and width
	{
        	unsigned char c=getchar();
                pic.push_back(c);//initializing entered picture
	}
	int length=(pic[22]+256)%256+((pic[23]+256)%256)*pow(2,8)+((pic[24]+256)%256)*pow(2,16)+((pic[25]+256)%256)*pow(2,24);//length of picture in byte 22 to 25
    int width=(pic[18]+256)%256+((pic[19]+256)%256)*pow(2,8)+((pic[20]+256)%256)*pow(2,16)+((pic[21]+256)%256)*pow(2,24);
	width*=3;//each pixel included 3 bytes
	zeros=0;
	while(width%4!=0){
		width++;
		zeros++;
	}
	for(int i=54;i<width*length+54;i++)//getting remain bytes
	{
                unsigned char c=getchar();
                pic.push_back(c);
        }
	int k=1;
	while(argv[k])//until any input exists
	{
		if(strcmp(argv[k],"1")==0)//reading from terminal and compare to do effects
			grayscale(pic,width);
		else if(strcmp(argv[k],"2")==0)
			mirror(pic,width);
		else if(strcmp(argv[k],"3")==0)
			rotate(pic);
		k++;
	}
	for(int i=0;i<pic.size();i++)
		cout<<pic[i];//printing result of effecting on picture
	return 0;
}
